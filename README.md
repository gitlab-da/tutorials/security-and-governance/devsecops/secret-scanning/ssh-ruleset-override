# SSH Ruleset Override

This project contains secret detection ruleset configuration which [overrides predefined analyzer rules](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#override-predefined-analyzer-rules) overrides the following rulesets to include more detailed information on the secret detected:

- RSA key
- SSH key
- PGP key

It is used by the [Misc SSH Keys project](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/secret-scanning/misc-ssh-keys) to show how to [specify a remote configuration file](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#specify-a-remote-configuration-file)

## References

- [Pipeline Secret Scanning Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/)

- [Custom Rulesets Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#custom-rulesets)

- [Feature proposal: Ability to include remote "custom ruleset"](https://gitlab.com/gitlab-org/gitlab/-/issues/336395)

- Project photo by [Mikhail Pavstyuk](https://unsplash.com/@pavstyuk?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/selective-focus-photography-of-three-books-beside-opened-notebook-EKy2OTRPXdw?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)